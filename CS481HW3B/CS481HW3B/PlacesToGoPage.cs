﻿using System;

using Xamarin.Forms;

namespace CS481HW3B
{
    public class PlacesToGoPage : ContentPage
    {
        public PlacesToGoPage()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}

